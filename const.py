DOMAIN = 'smarthome_voice_assistant'

DEV_LIST = ['switch', 'light', 'climate', 'cover', 'fan']
CONF_SELECT = 'select_all'

sub_topic_ha = 'sys/user/get/ha/'
sub_topic = 'sys/user/get/'
pub_topic = "sys/user/update/"

SET_COLOR = 'rgb_color'
NULL_VALUE = "null"
SET_COLOR_TEMPERATURE = 'color-temperature'
SET_BRIGHTNESS = 'brightness'

CONF_BROKER = 'hasskit.com' 
CONF_BROKER_PORT = 1883

AUTH_URL = 'https://hasskit.com/mqtt/auth'


PROPS = {
    'power': 'Power',
    'mode': 'Mode',
    'fan-level': 'FanSpeed',
    'color': 'Color',
    'target-temperature': 'TargetTemperature',
    'brightness': 'Brightness',
    'oscillate': 'Oscillate',
    'control': 'Control',
    'target-position':'TargetPosition',
    'current_position':'CurrentPosition'
}