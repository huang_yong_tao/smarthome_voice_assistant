from .const import PROPS


class GetProps:
    def __init__(self, state) -> None:
        self.pro_cls = None
        self._state = state

    def get_value(self, name):
        self.pro_cls = eval(PROPS[name])(self._state)
        return self.pro_cls.get_value()


class Parent:
    def __init__(self, state) -> None:
        self._state = state['state']
        self._attributes = state['attributes']
        self._domain = state['domain']


class Power(Parent):
    def __init__(self, state) -> None:
        super().__init__(state)

    def get_value(self):
        return False if self._state == 'off' else True


class TargetTemperature(Parent):
    def __init__(self, state) -> None:
        super().__init__(state)

    def get_value(self):
        return self._attributes['temperature']


class Mode(Parent):
    def __init__(self, state) -> None:
        super().__init__(state)

    def get_value(self):
        return self._attributes['hvac_modes'].index(self._state)


class FanSpeed(Parent):
    def __init__(self, state) -> None:
        super().__init__(state)

    def _get_climate_value(self):
        if self._attributes['fan_mode'] == None:
            self._attributes['fan_mode'] = 'auto'
        return self._attributes['fan_modes'].index(self._attributes['fan_mode'])

    def get_value(self):
        if self._domain == 'climate':
            return self._get_climate_value()
        if self._attributes['percentage'] > 60:
            return 3
        elif self._attributes['percentage'] <= 30:
            return 1
        else:
            return 2


class Color(Parent):
    def __init__(self, state) -> None:
        super().__init__(state)

    def get_value(self):
        if 'rgb_color' not in self._attributes:
            self._attributes['rgb_color'] = [255,0,0]
        if self._attributes['rgb_color'] == None:
            self._attributes['rgb_color'] = [255,0,0]
        return self._attributes['rgb_color']


class Brightness(Parent):
    def __init__(self, state) -> None:
        super().__init__(state)

    def get_value(self):
        if 'brightness' not in self._attributes:
            self._attributes['brightness'] = 125
        if self._attributes['brightness'] == None:
            self._attributes['brightness'] = 125     
        return self._attributes['brightness'] / 255 * 100

class Oscillate(Parent):
    def __init__(self, state) -> None:
        super().__init__(state)

    def get_value(self):
        return self._attributes['oscillating']

class CurrentPosition(Parent):
    """获取窗帘位置"""
    def __init__(self, state) -> None:
        super().__init__(state)
    def get_value(self):
        return self._attributes['current_position']

class TargetPosition(Parent):
    """获取窗帘set位置"""
    def __init__(self, state) -> None:
        super().__init__(state)
    def get_value(self):
        return self._attributes['current_position']
