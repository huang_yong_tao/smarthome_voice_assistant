from .const import PROPS


class SetServiceData:
    def __init__(self,obj) -> None:
        self.pro_cls = eval(PROPS[obj['name']])(obj)
    def set_call_service_data(self,payload):
        self.pro_cls.set_entity_id(payload)
        return self.pro_cls.set_call_service_data(payload)


class ServiceData:
    """ 调用服务数据实体类 """
    def __init__(self,domain) -> None:
        self._domain = domain
        self._service = ''
        self._service_data = {}
    @property
    def domain(self):
        return self._domain
    @domain.setter
    def domain(self,data):
        self._domain = data
    @property
    def service(self):
        return self._service
    @service.setter
    def service(self,data):
        self._service = data
    @property
    def service_data(self):
        return self._service_data
    @service_data.setter
    def service_data(self,data):
        self._service_data = data

class Parent:
    """ 公用类 """
    def __init__(self, obj ) -> None:
        self._domain = obj['id'].split('-')[3]
        self._val = obj['value']
        self._service_data = ServiceData(self._domain)
        self._service_data.service = 'turn_on'
    def chck_max_mix_data(self,max,min):
        """ 校验最大小最值 """
        if self._val >= max:
            return max
        if self._val <= min:
            return min
        return self._val

    def set_entity_id(self,payload):
        self._service_data.service_data['entity_id'] = payload['entity_id']
        


class Power(Parent):
    """ 处理电源开关 """
    def __init__(self, obj) -> None:
        super().__init__(obj)

    def set_climate_call_service_data(self, obj):
        """ 处理空调开关 """
        self._service_data.service_data['hvac_mode'] = 'auto'
        self._service_data.service = 'set_hvac_mode'
        return self._service_data

    def set_cover_call_service_data(self,obj):
        """处理窗帘开关"""
        if self._val == True:
            self._service_data.service = 'open_cover'
        elif self._val == False:
            self._service_data.service = 'close_cover'
        return self._service_data

    def set_call_service_data(self, payload):
        if self._domain == 'cover':
            return self.set_cover_call_service_data(payload)
        if not self._val:
            self._service_data.service = 'turn_off'
            return self._service_data
        if self._domain == 'climate':
            return self.set_climate_call_service_data(payload)
        return self._service_data


class TargetTemperature(Parent):
    """ 处理设置温度 """
    def __init__(self, obj) -> None:
        super().__init__(obj)
        self._max = 30
        self._min = 17

    def set_call_service_data(self, payload=None):
        data = self.chck_max_mix_data(self._max,self._min)
        self._service_data.service_data['temperature'] = data
        self._service_data.service = 'set_temperature'
        return self._service_data


class Mode(Parent):
    """ 处理模式 """
    def __init__(self, obj) -> None:
        super().__init__(obj)

    def set_call_service_data(self, payload=None):
        if self._domain != 'climate':
            return None
        data = payload['attributes']['hvac_modes'][self._val]
        self._service_data.service_data['hvac_mode'] = data
        self._service_data.service = 'set_hvac_mode'
        return self._service_data

class FanSpeed(Parent):
    """ 处理风速 """
    def __init__(self, obj) -> None:
        super().__init__(obj)
        self._max = 3
        self._min = 1
        self._fan_max = 3
        self._fan_min = 1

    def set_climate_fan_speed(self, payload=None):
        """ 处理空调风速 """
        data = self.chck_max_mix_data(self._max,self._min)
        data = payload['attributes']['fan_modes'][data]
        self._service_data.service_data['fan_mode'] = data
        self._service_data.service = 'set_fan_mode'
        return self._service_data

    def set_call_service_data(self, payload=None):
        if self._domain == 'climate':
            return self.set_climate_fan_speed(payload)

        """ 默认风扇风速 """
        data = self.chck_max_mix_data(self._fan_max,self._fan_min)
        if data == 3:
            data = 100
        elif data == 2:
            data = 60
        else: data = 30
        self._service_data.service_data['percentage'] = data
        self._service_data.service= 'set_percentage'
        return self._service_data


class Brightness(Parent):
    """ 处理亮度 """
    def __init__(self, obj) -> None:
        super().__init__(obj)

    def set_call_service_data(self, payload):
        data = self.chck_max_mix_data(100,1)
        self._service_data.service_data['brightness'] = data / 100 * 255
        return self._service_data

class Color(Parent):
    """ 处理颜色 """
    def __init__(self, obj) -> None:
        super().__init__(obj)

    def set_call_service_data(self, payload):
        self._service_data.service_data['rgb_color'] = self._val
        return self._service_data

class Oscillate(Parent):
    def __init__(self, obj) -> None:
        super().__init__(obj)
    
    def set_call_service_data(self,payload):
        self._service_data.service_data['oscillating'] = self._val
        self._service_data.service = 'oscillate'
        return self._service_data

class Control(Parent):
    """窗帘启停"""
    def __init__(self, obj) -> None:
        super().__init__(obj)
    
    def set_call_service_data(self,payload):
        if self._val == 1:
            self._service_data.service = 'open_cover'
        elif self._val == 2:
            self._service_data.service = 'close_cover'
        else:
            self._service_data.service = 'stop_cover'
        return self._service_data

class TargetPosition(Parent):
    """控制窗帘坐标"""
    def __init__(self, obj) -> None:
        super().__init__(obj)
    
    def set_call_service_data(self,payload):
        self._service_data.service = 'set_cover_position'
        self._service_data.service_data['position'] = self._val
        return self._service_data
