"""Utils for Molobot."""
import logging

import uuid

LOGGER = logging.getLogger(__package__)


def get_mac_addr():
    """Get local mac address."""
    import uuid
    node = uuid.getnode()
    mac = uuid.UUID(int=node).hex[-12:]
    return mac